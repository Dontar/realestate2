import "normalize.css";
import React from "react";
import ReactDom from "react-dom";
import { Layout } from "./layout/layout";

ReactDom.render(<Layout />, document.getElementById("app"));

if ("serviceWorker" in navigator) {
    window.addEventListener("load", () => {
        navigator.serviceWorker.register("/sw.js").then((registration) => {
            // tslint:disable-next-line:no-console
            console.log("SW registered: ", registration);
        }).catch((registrationError) => {
            // tslint:disable-next-line:no-console
            console.log("SW registration failed: ", registrationError);
        });
    });
}
