import { action, observable, runInAction } from "mobx";
import { observer } from "mobx-react";
import React, { Component } from "react";
import { FormattedMessage as FM } from "react-intl";
import ReactTable, { Column } from "react-table";
import "react-table/react-table.css";
import { BaseView } from "../share/base-view";
import { EcnButton } from "../share/buttons/ecn-button";

@observer
export class Space extends Component<any> {

    private columns: Column[] = [
        { Header: (<FM id="company_name" />), accessor: "company_name" },
        { Header: (<FM id="has_pict" />), accessor: "has_pict" },
        { Header: (<FM id="building_name" />), accessor: "building_name" },
        { Header: (<FM id="building_space_cnt" />), accessor: "building_space_cnt" },
        { Header: (<FM id="purchase_value" />), accessor: "purchase_value" },
        { Header: (<FM id="broker_commission" />), accessor: "broker_commission" },
        { Header: (<FM id="kreditvermittler_value" />), accessor: "kreditvermittler_value" },
        { Header: (<FM id="bank_fee" />), accessor: "bank_fee" },
        { Header: (<FM id="notary_commission" />), accessor: "notary_commission" },
        { Header: (<FM id="transfer_tax" />), accessor: "transfer_tax" },
        { Header: (<FM id="acquisition_value" />), accessor: "acquisition_value" },
        { Header: (<FM id="own_funds" />), accessor: "own_funds" },
        { Header: (<FM id="own_funds_perc" />), accessor: "own_funds_perc" },
        { Header: (<FM id="debt_capital" />), accessor: "debt_capital" },
        { Header: (<FM id="debt_capital_perc" />), accessor: "debt_capital_perc" },
        { Header: (<FM id="investment_costs" />), accessor: "investment_costs" },
        { Header: (<FM id="investment_costs_total" />), accessor: "investment_costs_total" },
        { Header: (<FM id="investment_costs_future" />), accessor: "investment_costs_future" },
        { Header: (<FM id="total_costs" />), accessor: "total_costs" },
        { Header: (<FM id="purchase_value_m2" />), accessor: "purchase_value_m2" },
        { Header: (<FM id="acquisition_value_m2" />), accessor: "acquisition_value_m2" },
        { Header: (<FM id="building_area" />), accessor: "building_area" },
        { Header: (<FM id="year_tenant_neto_rent" />), accessor: "year_tenant_neto_rent" },
        { Header: (<FM id="year_faktor" />), accessor: "year_faktor" },
        { Header: (<FM id="year_faktor_perc" />), accessor: "year_faktor_perc" },
        { Header: (<FM id="month_tenant_neto_rent_m2" />), accessor: "month_tenant_neto_rent_m2" },
        { Header: (<FM id="building_name" />), accessor: "building_name" },
        { Header: (<FM id="year_tenant_neto_rent_future" />), accessor: "year_tenant_neto_rent_future" },
        { Header: (<FM id="year_faktor_future" />), accessor: "year_faktor_future" },
        { Header: (<FM id="year_faktor_perc_future" />), accessor: "year_faktor_perc_future" },
        { Header: (<FM id="building_rent_m2" />), accessor: "month_tenant_neto_rent_m2_f" },
        { Header: (<FM id="building_rent_m2" />), accessor: "building_rent_m2" },
        { Header: (<FM id="sale_value" />), accessor: "sale_value" },
        { Header: (<FM id="sale_value_m2" />), accessor: "sale_value_m2" },
        { Header: (<FM id="diff_sale_value_m2" />), accessor: "diff_sale_value_m2" },
        { Header: (<FM id="building_space_cnt" />), accessor: "building_space_cnt" },
        { Header: (<FM id="space_type" />), accessor: "" },
        { Header: (<FM id="space_type_office" />), accessor: "" },
        { Header: (<FM id="date_of_acquisition" />), accessor: "date_of_acquisition" },
        { Header: (<FM id="site_name" />), accessor: "site_name" },
        { Header: (<FM id="address" />), accessor: "building_address" },
        { Header: (<FM id="manager_name" />), accessor: "manager_name" },
        { Header: (<FM id="object_manager_name" />), accessor: "object_manager_name" },
        { Header: (<FM id="broker_name" />), accessor: "broker_name" },
        { Header: (<FM id="date_of_sale" />), accessor: "date_of_sale" },
    ];

    @observable
    private loading: boolean = false;

    @observable
    private dataList: any[] = [];

    public render() {
        return (
            <BaseView
                title={<FM id="table_site" />}
                toolbar={
                    <div>
                        <EcnButton><FM id="add" /></EcnButton>
                        <EcnButton onClick={this.getData}>
                            <i className="material-icons">refresh</i>
                        </EcnButton>
                    </div>}
            >
                <ReactTable
                    showPagination={false}
                    loading={this.loading}
                    minRows={0}
                    columns={this.columns}
                    data={this.dataList.slice()}
                    style={{ flex: 1, overflow: "hidden" }}

                />
            </BaseView>
        );
    }

    @action
    private getData = () => {
        this.loading = true;
        fetch("/api/properties/get_list_space").then((r) => r.json()).then((r) => {
            runInAction(() => {
                this.dataList = r.data;
                this.loading = false;
            });
        });
    }

}
