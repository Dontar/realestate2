import { action, observable, runInAction } from "mobx";
import { observer } from "mobx-react";
import React, { Component } from "react";
import { FormattedMessage as FM } from "react-intl";
import ReactTable, { Column } from "react-table";
import "react-table/react-table.css";
import { BaseView } from "../share/base-view";
import { EcnButton } from "../share/buttons/ecn-button";

@observer
export class Portfolio extends Component<any> {

    private columns: Column[] = [
        { Header: (<FM id="name" />), accessor: "site_name", minWidth: 300 },
        { Header: (<FM id="table_building"></FM>), accessor: "site_building_cnt" },

    ];

    @observable
    private loading: boolean = false;

    @observable
    private dataList: any[] = [];

    public render() {
        return (
            <BaseView
                title={<FM id="table_site" />}
                toolbar={
                    <div>
                        <EcnButton><FM id="add" /></EcnButton>
                        <EcnButton onClick={this.getData}>
                            <i className="material-icons">refresh</i>
                        </EcnButton>
                    </div>}
            >
                <ReactTable
                    showPagination={false}
                    loading={this.loading}
                    minRows={0}
                    columns={this.columns}
                    data={this.dataList.slice()}
                    style={{ flex: 1 }}

                />
            </BaseView>
        );
    }

    @action
    private getData = () => {
        this.loading = true;
        fetch("/api/properties/site_ajax").then((r) => r.json()).then((r) => {
            runInAction(() => {
                this.dataList = r;
                this.loading = false;
            });
        });
    }

}
