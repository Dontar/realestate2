var popo = [{
		title: "{#company_name#}",
		data: 'company_name',
		className: "ellipsis"
	},
	{
		title: "{#has_pict#}",
		data: 'has_pict',
		className: "dt-center td-no-padding",
	},

	{
		title: "{#building_name#}",
		data: 'building_name',
		className: "td-no-padding",
	},
	{
		title: "{#building_space_cnt#}",
		name: 'building_space_cnt',
		data: 'building_space_cnt',
		className: "sum_footer_cnt dt-center td-no-padding",
	},

	// Следващите 6 колони трябва да могат да се скриват и показват
	{
		title: "{#purchase_value#}",
		name: 'purchase_value',
		data: 'purchase_value',
		visible: false,
		className: "to_hide sum_footer_big dt-right",
	},
	{
		title: "{#broker_commission#}",
		name: 'broker_commission',
		data: 'broker_commission',
		visible: false,
		className: "to_hide sum_footer_big dt-right",
	},
	{
		title: "{#kreditvermittler_value#}",
		name: 'kreditvermittler_value',
		data: 'kreditvermittler_value',
		visible: false,
		className: "to_hide sum_footer_big dt-right",
	},
	{
		title: "{#bank_fee#}",
		name: 'bank_fee',
		data: 'bank_fee',
		visible: false,
		className: "to_hide sum_footer_big dt-right",
	},
	{
		title: "{#notary_commission#}",
		name: 'notary_commission',
		data: 'notary_commission',
		visible: false,
		className: "to_hide sum_footer_big dt-right",
	},
	{
		title: "{#transfer_tax#}",
		name: 'transfer_tax',
		data: 'transfer_tax',
		visible: false,
		className: "to_hide sum_footer_big dt-right",
	},

	// Gesamt Kaufpreis-summe = sum(горните 6)
	{
		title: "{#acquisition_value#}",
		name: 'acquisition_value',
		data: 'acquisition_value',
		className: "sum_footer_big dt-right",
	},

	// Eigenkapital
	{
		title: "{#own_funds#}",
		name: 'own_funds',
		data: 'own_funds',
		className: "sum_footer_big dt-right",
	},

	// Eigenkapital in % = own_funds * 100 / acquisition_value = own_funds_perc
	{
		title: "{#own_funds_perc#}",
		name: 'own_funds_perc',
		data: 'own_funds_perc',
		className: "dt-right",
	},

	// Gesamt Kreditsumme
	{
		title: "{#debt_capital#}",
		name: 'debt_capital',
		data: 'debt_capital',
		className: "sum_footer_big dt-right",
	},

	// Kreditsumme in % = debt_capital * 100 / acquisition_value = debt_capital_perc
	{
		title: "{#debt_capital_perc#}",
		name: 'debt_capital_perc',
		data: 'debt_capital_perc',
		className: "dt-right",
	},

	// Investitionskosten - Инвестиционни разходи до момента
	// investment_costs = sum(ir_line.ir_line_amount * (100+ir_line.ir_line_vat_rate)/100) where ir_line.ir_line_is_acquisition <> '1' and charge.charge_is_capex = '1' and ir.ir_invdate <= current_date
	{
		title: "{#investment_costs#}",
		name: 'investment_costs',
		data: 'investment_costs',
		className: "sum_footer_big dt-right",
	},

	// Investitionskosten gesamt = acquisition_value + investment_costs
	// Инвестиционни разходи общо с бъдещите - Future investment costs
	{
		title: "{#investment_costs_total#}",
		name: 'investment_costs_total',
		data: 'investment_costs_total',
		className: "sum_footer_big dt-right",
	},

	// Invetionskosten Soll - получава се като сума от бъдещите инвестиционни разходи по неприключените ремонти
	// ако не е приключен ремонта, то се взима бюджета и от него се вади платената сума по фактурите
	// Инвестиционни разходи за в бъдеще - Total investment costs
	{
		title: "{#investment_costs_future#}",
		name: 'investment_costs_future',
		data: 'investment_costs_future',
		className: "sum_footer_big dt-right",
	},

	// Gesamtkosten Soll - Общо разходи = investment_costs_total + investment_costs_future
	{
		title: "{#total_costs#}",
		name: 'total_costs',
		data: 'total_costs',
		className: "sum_footer_big dt-right",
	},

	// Kaufpreis m² Ankauf = purchase_value / building_area
	{
		title: "{#purchase_value_m2#}",
		name: 'purchase_value_m2',
		data: 'purchase_value_m2',
		className: "dt-right",

	},

	// Kaufpreis m² Aktuell = acquisition_value / building_area
	// 15.09.2016 = acquisition_value + investment_costs / building_area = investment_costs_total / building_area
	{
		title: "{#acquisition_value_m2#}",
		name: 'acquisition_value_m2',
		data: 'acquisition_value_m2',
		className: "dt-right",

	},

	// m² des Objektes
	{
		title: "{#building_area#}",
		name: 'building_area',
		data: 'building_area',
		className: "sum_footer_cnt dt-right",

	},

	// Ist Jahresmiete - текущ годишен наем = sum(tenant_neto_rent + tenant_rent_mod) * 12
	{
		title: "{#year_tenant_neto_rent#}",
		name: 'year_tenant_neto_rent',
		data: 'year_tenant_neto_rent',
		className: "sum_footer_big dt-right",

	},

	// Ist Faktor - текуща възращаемост в години =
	// 11.03.2016 - investment_costs_total / year_tenant_neto_rent
	{
		title: "{#year_faktor#}",
		name: 'year_faktor',
		data: 'year_faktor',
		className: "dt-right",

	},

	// Rendite in % - рентабилност = 100 / year_faktor
	{
		title: "{#year_faktor_perc#}",
		name: 'year_faktor_perc',
		data: 'year_faktor_perc',
		className: "dt-right",

	},

	// Ist pro m² - текущ месечен наем на м2 = year_tenant_neto_rent / 12 / building_area
	{
		title: "{#month_tenant_neto_rent_m2#}",
		name: 'month_tenant_neto_rent_m2',
		data: 'month_tenant_neto_rent_m2',
		className: "dt-right",

	},

	{
		title: "{#building_name#}",
		data: 'building_name',
		className: "td-no-padding",

	},

	// Soll Jahresmiete - бъдещ годишен наем = sum(tenant_neto_rent + tenant_rent_mod + neto_rent_soll_diff) * 12
	{
		title: "{#year_tenant_neto_rent_future#}",
		name: 'year_tenant_neto_rent_future',
		data: 'year_tenant_neto_rent_future',
		className: "sum_footer_big dt-right",

	},

	// Soll Faktor - бъдеща възращаемост в години =
	// 11.03.2016 - total_costs / year_tenant_neto_rent_future
	{
		title: "{#year_faktor_future#}",
		name: 'year_faktor_future',
		data: 'year_faktor_future',
		className: "dt-right",

	},

	// Soll Rendite in % - бъдеща рентабилност = 100 / year_faktor
	{
		title: "{#year_faktor_perc_future#}",
		name: 'year_faktor_perc_future',
		data: 'year_faktor_perc_future',
		className: "dt-right",

	},

	// Soll pro m² - бъдещ месечен наем на м2 = building_rent_m2 = year_tenant_neto_rent_future / 12 / building_area
	{
		title: "{#building_rent_m2#}",
		name: 'month_tenant_neto_rent_m2_f',
		data: 'month_tenant_neto_rent_m2_f',
		className: "dt-right",

	},

	// Soll pro m² - взето от building.building_rent_m2
	{
		title: "{#building_rent_m2#}",
		name: 'building_rent_m2',
		data: 'building_rent_m2',
		className: "dt-right",

	},

	// Verkauft Продаден
	{
		title: "{#sale_value#}",
		name: 'sale_value',
		data: 'sale_value',
		className: "sum_footer_big dt-right",

	},

	// Verkaufpreis m² - продажна цена на m2 = sale_value / building_area
	{
		title: "{#sale_value_m2#}",
		name: 'sale_value_m2',
		data: 'sale_value_m2',
		className: "dt-right",

	},

	// Differenz Betrag EK zu VK = Verkaufpreis m² - Kaufpreis m² = sale_value_m2 - acquisition_value_m2
	{
		title: "{#diff_sale_value_m2#}",
		name: 'diff_sale_value_m2',
		data: 'diff_sale_value_m2',
		className: "dt-right",

	},

	{
		title: "{#building_space_cnt#}",
		name: 'building_space_cnt',
		data: 'building_space_cnt',
		className: "sum_footer_cnt dt-center td-no-padding",
	},
	{
		title: "{#space_type#}",
		name: 'space_type',
		data: '',
		className: "dt-left",
	},
	{
		title: "{#space_type_office#} m²",
		name: 'space_type_office',
		data: '',
		className: "dt-right",
	},

	{
		title: "{#date_of_acquisition#}",
		data: 'date_of_acquisition',

	},
	//{ title: "{#potential_new_space#}", name: 'potential_new_space', data: 'potential_new_space', className: "sum_footer_qty dt-right",
	{
		title: "{#site_name#}",
		data: 'site_name'
	},
	{
		title: "{#address#}",
		data: 'building_address'
	},
	{
		title: "{#manager_name#}",
		data: 'manager_name'
	},
	{
		title: "{#object_manager_name#}",
		data: 'object_manager_name'
	},
	{
		title: "{#broker_name#}",
		data: 'broker_name'
	},
	{
		title: "{#date_of_sale#}",
		data: 'date_of_sale',

	}
];

popo.forEach((item) => {
	var res = /\{\#(.+)\#\}/.exec(item.title);
	console.log(`{ Header: (<FM id="${res[1]}" />), accessor: "${item.data}"}`);
});
