import { action, observable } from "mobx";
import deLang from "./intl/de.lang.json";
import enLang from "./intl/en.lang.json";

interface IMessages { [name: string]: string; }

export class MainStore {

    @observable
    public loggedIn: boolean = false;

    @observable
    public lang: string = "en";

    @observable
    public sidebarOpen: boolean = true;

    @observable
    public error: string | undefined;

    public msg: IMessages | null = null;

    constructor() {
        this.changeLang(localStorage.getItem("lang") || "en");
    }

    @action
    public changeLang(lang: string) {
        if (lang !== this.lang) {
            fetch(lang === "en" ? enLang : deLang).then((r) => r.json()).then(action((r) => {
                this.msg = r as IMessages;
                localStorage.setItem("lang", lang);
                this.lang = lang;
            }));
        }
    }

    @action
    public toggleSidebar() {
        this.sidebarOpen = !this.sidebarOpen;
    }

    @action
    public login(user: string, pass: string) {
        const data = new FormData();
        data.append("login_user", user);
        data.append("login_pass", pass);
        fetch("/api/main_menu/login", { body: data, method: "POST" }).then((r) => {
            const res = r.json();
            return res;
        }).then(action((r) => {
            this.loggedIn = r === 1;
            if (!this.loggedIn) {
                this.error = "Wrong username or password!!!";
                setTimeout(action(() => {
                    this.error = undefined;
                }), 10000);
            }
        })).catch((e) => {
            // tslint:disable-next-line:no-console
            console.error(e);
        });
    }

    @action
    public logout() {
        this.loggedIn = false;
    }
}

export const mainStore = new MainStore();
