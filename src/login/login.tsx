import React, { Component } from "react";
import { EcnButton } from "../share/buttons/ecn-button";
import { EcnEditor } from "../share/editors/ecn-editor";

import "./login.less";

import { MainStore } from "../mainStore";

import { inject, observer } from "mobx-react";

@inject("mainStore")
@observer
export class Login extends Component<{ mainStore?: any }, any> {
    private theUser: EcnEditor | null = null;
    private thePass: EcnEditor | null = null;
    private store: MainStore;

    constructor(props: any, context: any) {
        super(props, context);
        this.store = this.props.mainStore;
    }
    public render() {
        return (
            <div className="login-wrap">
                <div className="login-panel">
                    <div className="logo"></div>
                    <div className="error" style={{ display: this.store.error ? "block" : "none" }}>
                        {this.store.error || ""}
                    </div>
                    <EcnEditor
                        ref={(el) => this.theUser = el} className="user" type="text" placeholder="Username..."
                    />
                    <EcnEditor
                        ref={(el) => this.thePass = el} className="pass" type="password" placeholder="Password..."
                    />
                    <EcnButton className="login" onClick={this.onLogin}>Login</EcnButton>
                    <span className="lang">
                        <a
                            className={`en ${this.store.lang === "en" ? "active" : ""}`}
                            href="javascript:;" onClick={this.onEnClick}
                        >
                            EN
                        </a>
                        <a
                            className={`en de ${this.store.lang === "de" ? "active" : ""}`}
                            href="javascript:;" onClick={this.onDeClick}
                        >
                            DE
                        </a>
                    </span>
                    <a className="recover" href="javascript:;">Recover password</a>
                </div>
            </div>
        );
    }

    private onLogin = () => {
        if (
            (this.theUser && this.theUser.value) &&
            (this.thePass && this.thePass.value)
        ) {
            this.store.login(this.theUser.value, this.thePass.value);
        }
    }

    private onEnClick = () => {
        this.store.changeLang("en");
    }

    private onDeClick = () => {
        this.store.changeLang("de");
    }

}
