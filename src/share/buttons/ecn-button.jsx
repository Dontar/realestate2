import * as tslib_1 from "tslib";
import React from "react";
import "./ecn-button.less";
export const EcnButton = (props) => {
    const _a = props ? props : { className: "", children: [] }, { className } = _a, prop = tslib_1.__rest(_a, ["className"]);
    return (<button className={"ecn-button " + className} {...prop}>{prop.children}</button>);
};
export const EcnButtonLink = (props) => {
    const _a = props ? props : { className: "", children: [] }, { className } = _a, prop = tslib_1.__rest(_a, ["className"]);
    return (<a className={"ecn-button " + className} {...prop}>{prop.children}</a>);
};
