import React, { AllHTMLAttributes } from "react";

import "./ecn-button.less";

export const EcnButton = (props?: AllHTMLAttributes<HTMLButtonElement>) => {
    const { className, ...prop } = props ? props : { className: "", children: [] };
    return (
        <button className={"ecn-button " + className} {...prop}>{prop.children}</button>
    );
};

export const EcnButtonLink = (props?: AllHTMLAttributes<HTMLAnchorElement>) => {
    const { className, ...prop } = props ? props : { className: "", children: [] };
    return (
        <a className={"ecn-button " + className} {...prop}>{prop.children}</a>
    );
};
