import React, { Component, MouseEventHandler, ReactNode } from "react";

import "./base-view.less";

import { EcnButton } from "./buttons/ecn-button";
import { EcnSearchBox } from "./editors/ecn-editor";

import { action, observable } from "mobx";
import { observer } from "mobx-react";

interface IBaseViewProps {
    cls: string;
    title: ReactNode;
    toolbar: ReactNode;
    filter: ReactNode;
}

@observer
export class BaseView extends Component<Partial<IBaseViewProps>> {

    @observable
    private filterOpened: boolean = false;

    public render() {
        return (
            <div className={`base-view ${this.props.cls}`}>
                <div className="title">
                    <b> Step1 > Step2 </b>
                    {this.props.title}
                </div>
                <div className="toolbar">
                    {this.props.toolbar}
                    <div style={{ flex: "1" }}></div>
                    <EcnSearchBox />
                    {this.props.filter ? (
                        <EcnButton onClick={this.onFilterOpen}><i className="material-icons">filter</i></EcnButton>
                    ) : ("")}

                </div>
                <div className={`filter ${this.filterOpened ? "open" : ""}`}>
                    {this.props.filter}
                </div>
                <div className="content">
                    {this.props.children}
                </div>
            </div>
        );
    }

    @action
    private onFilterOpen: MouseEventHandler<HTMLButtonElement> = (ev) => {
        this.filterOpened = !this.filterOpened;
    }
}
