declare module "clean-webpack-plugin";

declare module "*.json";

declare module "workbox-webpack-plugin" {
    export interface IWorkboxConfig {
        clientsClaim: boolean;
        skipWaiting: boolean;
    }
    export class GenerateSW {
        constructor(config: IWorkboxConfig);
    }
}
