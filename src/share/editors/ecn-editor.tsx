import { action, observable } from "mobx";
import { observer } from "mobx-react";
import React, { AllHTMLAttributes, ChangeEventHandler, Component } from "react";

import "./ecn-editor.less";

interface IEditorProps<T> extends AllHTMLAttributes<T> {
    inputClass: string;
    label: string;
}

export class EcnEditor extends Component<Partial<IEditorProps<HTMLInputElement>>, any> {
    private theInput: HTMLInputElement | null = null;

    get value() {
        return this.theInput ? this.theInput.value : null;
    }

    public render() {
        const { className, inputClass, label, id, ...prop } =
            this.props ? this.props : { className: "", inputClass: "", label: null, id: "" };
        return (
            <div className={"ecn-editor " + className}>
                {label ? (<label htmlFor={id}>{label}</label>) : ""}
                <input ref={(el) => this.theInput = el} className={inputClass} {...prop} />
            </div>
        );
    }
}

// tslint:disable-next-line:max-classes-per-file
export class EcnTextarea extends Component<Partial<IEditorProps<HTMLTextAreaElement>>, any> {
    private theInput: HTMLTextAreaElement | null = null;

    get value() {
        return this.theInput ? this.theInput.value : null;
    }

    public render() {
        const { className, inputClass, label, id, children, ...prop } =
            this.props ? this.props : { className: "", inputClass: "", label: null, id: "", children: [] };
        return (
            <div className={"ecn-editor " + className}>
                {label ? (<label htmlFor={id}>{label}</label>) : ""}
                <textarea ref={(el) => this.theInput = el} className={inputClass} {...prop}>{children}</textarea>
            </div>
        );
    }
}

// tslint:disable-next-line:max-classes-per-file
@observer
export class EcnSearchBox extends Component<Partial<IEditorProps<HTMLInputElement>>, any> {

    @observable
    private value: string = "";

    public render() {
        const { className, inputClass, label, id, placeholder, ...prop } =
            this.props ? this.props : { className: "", inputClass: "", label: null, id: "", placeholder: "" };
        return (
            <div className={"ecn-editor search-box " + className}>
                {label ? (<label htmlFor={id}>{label}</label>) : ""}
                <input
                    className={inputClass}
                    placeholder="Search..." {...prop}
                    onChange={this.onChange}
                    value={this.value}
                />
                <button className={this.value.length ? "clr" : ""} onClick={this.onClr}>x</button>
                <button>&nbsp;</button>
            </div>
        );
    }

    @action
    private onClr = () => {
        this.value = "";
    }

    @action
    private onChange: ChangeEventHandler<HTMLInputElement> = (ev) => {
        this.value = ev.target.value;
    }

}
