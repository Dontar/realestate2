import React, { Component, MouseEvent } from "react";

import "./header.less";

import { action, observable } from "mobx";
import { inject, observer } from "mobx-react";
import { MainStore } from "../mainStore";

interface IHeaderProps {
    mainStore?: any;
}

@inject("mainStore")
@observer
export class Header extends Component<IHeaderProps, any> {

    @observable
    private menuOpen: boolean = false;

    private store: MainStore = this.props.mainStore;

    public componentDidMount() {
        document.addEventListener("click", this.closeMenu);
    }

    public componentWillUnmount() {
        this.closeMenu();
        document.removeEventListener("click", this.closeMenu);
    }

    public render() {
        return (
            <div className="header">
                <a onClick={this.onSidebar} className="sidebar-toggle" href="javascript:;"></a>
                <div className="logo"></div>
                <div style={{ flex: "1" }}></div>
                <div className="logo2"></div>
                <a href="javascript:;" onClick={this.onMenuClick}>
                    <i className="material-icons user-placeholder">account_circle</i>
                </a>

                <ul className={`user-menu ${this.menuOpen ? "open" : ""}`}>
                    <li>
                        <i className="material-icons user-placeholder">account_circle</i>
                        <div className="user-title">
                            <b>Kaloyan Arsov</b><br />
                            ka@mail.com
                    </div>
                    </li>
                    <li className="menu-separator"></li>
                    <li>
                        <a
                            onClick={this.onEnClick}
                            className={`en ${this.langIsEn(this.store.lang)}`}
                            href="javascript:;">EN</a>
                    </li>
                    <li>
                        <a
                            onClick={this.onDeClick}
                            className={`en de ${this.langIsDe(this.store.lang)}`}
                            href="javascript:;">DE</a>
                    </li>
                    <li>
                        <a onClick={this.onLogout} href="javascript:;" className="exit">Logout</a>
                    </li>
                </ul>

            </div>
        );
    }

    @action
    private onMenuClick = (ev: MouseEvent<any>) => {
        this.menuOpen = !this.menuOpen;
        ev.stopPropagation();
    }

    private onLogout = () => {
        this.store.logout();
    }

    @action
    private closeMenu = () => {
        this.menuOpen = false;
    }

    private onEnClick = () => {
        this.store.changeLang("en");
    }

    private onDeClick = () => {
        this.store.changeLang("de");
    }

    private onSidebar = () => {
        this.store.toggleSidebar();
    }

    private langIsDe(lang: string) {
        return lang === "de" ? "active" : "";
    }

    private langIsEn(lang: string) {
        return lang === "en" ? "active" : "";
    }
}
