import React, { Component } from "react";
import { BrowserRouter as Router, Route, RouteProps } from "react-router-dom";

import { inject, observer, Provider } from "mobx-react";

import "./layout.less";

import { Login } from "../login/login";
import { Header } from "./header";
import { Sidebar } from "./sidebar";

import { MainStore, mainStore } from "../mainStore";

import { addLocaleData, IntlProvider } from "react-intl";

import * as de from "react-intl/locale-data/de";

import { Building } from "../properties/building";
import { Portfolio } from "../properties/portfolio";

addLocaleData(de);

@inject("mainStore")
@observer
class LayoutInt extends Component<{ mainStore?: any }, any> {
    public render() {
        const s: MainStore = this.props.mainStore;
        return (
            <IntlProvider locale={s.lang} messages={s.msg} key={s.lang}>
                {s.loggedIn ? (
                    <div className={`main-layout ${!s.sidebarOpen ? "close" : ""}`}>
                        <div className="left-line" />
                        <Header />
                        <Sidebar />
                        <div className="main">
                            {this.props.children}
                        </div>
                    </div>

                ) : (<Login />)}
            </IntlProvider>
        );
    }
}

export const Layout = () =>
    <Router>
        <div style={{width: "100%", height: "100%"}}>
            <Route path="/" component={(props: RouteProps) =>
                <Provider mainStore={mainStore}>
                    <LayoutInt>{props.children}</LayoutInt>
                </Provider>
            }>
            </Route>
            <Route path="/properties/site" component={Portfolio} />
            <Route path="/properties/building" component={Building} />
            <Route path="/properties/space" component={() => <div>Units</div>} />
        </div>
    </Router>
    ;
