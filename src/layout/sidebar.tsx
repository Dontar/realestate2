import React, { Component, MouseEvent } from "react";
import { FormattedMessage as FM } from "react-intl";
import ScrollArea from "react-scrollbar";
import "./sidebar.less";

import { action, observable } from "mobx";
import { observer } from "mobx-react";

import { Link } from "react-router-dom";

interface IMenu {
    icon: string;
    label: string;
    secure: any;
    href: string;
    sec: Array<{ href: string; label: string; secure: any; }>;
}

const menu: { [name: string]: IMenu } = {
    buildings: {
        href: "/properties/building",
        icon: "link2",
        label: "menu_properties",
        sec: [
            { href: "/properties/site", label: "table_site", secure: "$g.site" },
            { href: "/properties/building", label: "table_building", secure: "$g.building" },
            { href: "/properties/space", label: "table_space", secure: "$g.space" },
        ],
        secure: "$g.building",
    },
    tenant: {
        href: "/properties/space_tenant",
        icon: "link16",
        label: "menu_tenant",
        sec: [
            { href: "/properties/space_tenant", label: "table_space_tenant", secure: "$g.space" },
            { href: "/properties/vacating", label: "menu_vacating", secure: "$g.vacating000" },
            {
                href: "/properties/vacating_successfully",
                label: "menu_vacating_successfully",
                secure: "$g.vacating_successfully",
            },
        ],
        secure: "$g.tenant",
    },
    vacancy: {
        href: "/properties/vacancy",
        icon: "link15",
        label: "menu_vacancy",
        sec: [
            { href: "/properties/broker", label: "menu_broker", secure: "$g.broker" },
            {
                href: "/properties/broker_successfully",
                label: "menu_broker_successfully",
                secure: "$g.broker_successfully",
            },
        ],
        secure: "$g.vacancy",
    },
    // tslint:disable-next-line:object-literal-sort-keys
    renovation: {
        href: "/renovation/r_temp",
        icon: "link4",
        label: "menu_r_temp",
        sec: [
            { href: "/renovation/r_temp", label: "ongoing_renovations", secure: "$g.r_temp" },
            { href: "/renovation/r_contract", label: "ongoing_contracts", secure: "$g.r_contract" },
            { href: "/renovation_firm/r_contract", label: "all_contracts", secure: "$g.r_contract_all" },
            {
                href: "/renovation/r_temp_successfully",
                label: "menu_r_temp_successfully",
                secure: "$g.r_temp_successfully",
            },
        ],
        secure: "$g.r_temp",
    },
    ir: {
        href: "/ir/ir",
        icon: "link10",
        label: "financial_accounting",
        sec: [
            { href: "/ir/ir", label: "menu_ir", secure: "$g.ir" },
            { href: "/sepa_export/export", label: "menu_sepa_export", secure: "$g.sepa_export" },
            { href: "/financial_accounting/company", label: "mt940_import", secure: "$g.mt940_company" },
            { href: "/invoice/invoice", label: "menu_invoice", secure: "$g.invoice" },
            { href: "/datev/", label: "menu_datev_export", secure: "$g.datev" },
        ],
        secure: 1,
    },
    cost_overview: {
        href: "/ir/investment_costs",
        icon: "link9",
        label: "cost_overview",
        sec: [
            { href: "/ir/investment_costs", label: "investment_costs", secure: 1 },
            { href: "/ir/operating_costs", label: "operating_costs", secure: 1 },
        ],
        secure: 1,
    },
    financing: {
        href: "/financing/financing",
        icon: "link11",
        label: "menu_financing",
        sec: [
            { href: "/financing/kredit", label: "table_kredit", secure: "$g.kredit" },
            { href: "/financing/h_agreement", label: "menu_h_agreement", secure: "$g.h_agreement" },
            { href: "/financing/h_credit_sum", label: "h_credit_sum", secure: "$g.h_credit_sum" },
            { href: "/financing/h_credit", label: "table_h_credit", secure: "$g.h_credit" },
            {
                href: "/financing/h_credit_accrued_interest",
                label: "h_credit_accrued_interest",
                secure: "$g.h_credit_acc",
            },
        ],
        secure: "$g.financing",
    },
    Economic_Indicators: {
        href: "/Economic_Indicators/Calculation_of_Profitability",
        icon: "link12",
        label: "Economic_Indicators",
        sec: [
            {
                href: "/Economic_Indicators/Calculation_of_Profitability",
                label: "Calculation_of_Profitability",
                secure: "$g.Calculation_of_Profitability",
            },
            {
                href: "/Economic_Indicators/Calculation_of_Market_Value",
                label: "Calculation_of_Market_Value",
                secure: "$g.Calculation_of_Market_Value",
            },
            {
                href: "/Economic_Indicators/Actual_figures_and_Targets",
                label: "Actual_figures_and_Targets",
                secure: "$g.Actual_figures_and_Targets",
            },
            {
                href: "/Economic_Indicators/Sold_Properties",
                label: "Sold_Properties",
                secure: "$g.Sold_Properties",
            },
        ],
        secure: "$g.Economic_Indicators",
    },
    report_invests: {
        href: "/properties/report_invests",
        icon: "link17",
        label: "menu_report",
        sec: [
            { href: "/properties/report_invests_bank", label: "menu_report_bank", secure: "$g.report_invests_bank" },
        ],
        secure: "$g.report_invests",
    },
    documents: {
        href: "/documents",
        icon: "link14",
        label: "menu_documents",
        sec: [
            { href: "/documents/folder", label: "folder_name", secure: "$g.folder" },
        ],
        secure: 1,
    },
    all_contacts: {
        href: "/configuration/org",
        icon: "link13",
        label: "menu_renovation_contract",
        sec: [],
        secure: "$g.renovation_firm",
    },
    objectmanager: {
        href: "/objekt_manager/objekt_manager",
        icon: "link8",
        label: "menu_objectmanager",
        sec: [
            { href: "/objekt_manager/r_temp", label: "table_r_temp", secure: "$g.r_temp" },
            { href: "/objekt_manager/bonuses", label: "Bonuses", secure: "$g.bonuses" },
        ],
        secure: "$g.objekt_manager",
    },
    configuration: {
        href: "/configuration/r_service",
        icon: "link7",
        label: "menu_configuration",
        sec: [
            { href: "/configuration/r_service", label: "table_r_service", secure: "$g.r_service" },
            { href: "/configuration/r_service_group", label: "table_r_service_group", secure: "$g.r_service_group" },
            { href: "/configuration/uom", label: "table_uom", secure: "$g.uom" },
            { href: "/configuration/r_service_firm", label: "table_r_service_firm", secure: "$g.r_service_firm" },
            { href: "/configuration/company", label: "table_company", secure: "$g.company" },
            { href: "/configuration/room", label: "table_room", secure: "$g.room" },
            { href: "/configuration/doc_kind", label: "table_doc_kind", secure: "$g.doc_kind" },
            { href: "/configuration/charge", label: "table_charge", secure: "$g.charge" },
            { href: "/configuration/vat", label: "table_vat", secure: "$g.vat" },
            { href: "/configuration/personal", label: "table_personal", secure: "$g.personal" },
            { href: "/configuration/user", label: "table_user", secure: "$g.user" },
            { href: "/configuration/user_role", label: "table_user_role", secure: "$g.user" },
            { href: "/configuration/sttlp", label: "menu_sttlp", secure: "$g.sttlp" },
            { href: "/configuration/config", label: "table_config", secure: "$g.config" },
            { href: "/main_menu/languages", label: "languages", secure: "$g.languages" },
            { href: "/sys_reports/sys_oper", label: "table_sys_oper", secure: "$g.sys_oper && $g.sys_reports" },
            { href: "/sys_reports/sys_logon", label: "table_sys_logon", secure: "$g.sys_logon && $g.sys_reports" },
            {
                href: "/sys_reports/sys_logon1",
                label: "table_sys_logon",
                secure: "$g.$smarty.session.userdata.user_id && $g.sys_reports",
            },
        ],
        secure: "$g.configuration",
    },
};

@observer
export class Sidebar extends Component<any, any> {

    @observable
    private selectedMenu: number = -1;

    public render() {
        return (
            <ScrollArea className="sidebar" smoothScrolling={true}>
                <ul className="main-menu">
                    {Object.keys(menu).map((item: string, idx: number) => {
                        const i = menu[item];
                        return (
                            <li key={idx}>
                                <Link
                                    onClick={this.onMenuToggle.bind(this, idx)}
                                    className={`icon ${i.icon} ${this.selectedMenu === idx ? "active" : ""}`}
                                    to={i.href}
                                >
                                    <FM id={i.label} />
                                </Link>
                                <ul>
                                    {i.sec.map((ss) => {
                                        return (
                                            <li className="title">
                                                <Link
                                                    onClick={(ev) => { ev.preventDefault(); }}
                                                    to={ss.href}
                                                >
                                                    <FM id={ss.label} />
                                                </Link>
                                            </li>
                                        );
                                    })}
                                </ul>
                            </li>
                        );
                    })}
                </ul>
            </ScrollArea>
        );
    }

    @action
    private onMenuToggle = (selMenu: number, ev: MouseEvent<HTMLAnchorElement>) => {
        this.selectedMenu = (this.selectedMenu === selMenu ? -1 : selMenu);
        ev.preventDefault();
        ev.stopPropagation();
    }

}
