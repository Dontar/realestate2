import { mainStore } from "../src/mainStore";

describe("mainStore tests", () => {
    it("testing changeLang", () => {
        mainStore.changeLang("de");
        expect(mainStore.lang).toBe("de");
        expect(mainStore.msg).toBeTruthy();
    });
});
