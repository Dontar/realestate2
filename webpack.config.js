"use strict";

var CleanWebpackPlugin = require("clean-webpack-plugin");
var HtmlWebpackPlugin = require("html-webpack-plugin");
var path = require("path");
var WorkboxWebpackPlugin = require("workbox-webpack-plugin");

var buildPath = path.resolve(__dirname, "dist");

var config = {
    entry: "./src/index.tsx",
    mode: process.env.WEBPACK_SERVE ? "development" : "production",
    output: {
        filename: "bundle.[hash].js",
        path: buildPath
    },
    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",
    resolve: {
        alias: {
            "fixed-data-table": "fixed-data-table-2"
        },
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js"]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "assets/index.html",
            title: "Estate Control Network"
        }),
        new CleanWebpackPlugin(buildPath),
        new WorkboxWebpackPlugin.GenerateSW({
            // these options encourage the ServiceWorkers to get in there fast
            // and not allow any straggling "old" SWs to hang around
            clientsClaim: true,
            skipWaiting: true
        }),
    ],
    module: {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            { test: /\.tsx?$/, use: "awesome-typescript-loader" },
            // { test: /\.js$/, use: "source-map-loader" },
            { test: /\.less$/, use: ["style-loader", "css-loader", "less-loader"] },
            { test: /\.css$/, use: ["style-loader", "css-loader"] },
            { test: /\.(png|jpg|gif|svg)$/, use: ["url-loader"] },
            { test: /lang\.json$/, use: ["file-loader"], type: "javascript/auto" },
        ]
    },
    target: "web"
};
module.exports = config;
